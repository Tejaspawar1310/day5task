﻿using Day5Assignment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5Assignment.Repository
{
    internal interface IFile
    {
        void WriteContentsToFile(User user, string fileName);
        List<string> ReadContentsFromFile(string fileName);
        bool IsUsernameExists(string userName, string filename);

    }
}
