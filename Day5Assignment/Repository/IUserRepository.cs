﻿using Day5Assignment.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5Assignment.Repository
{
    internal interface IUserRepository
    {
        bool RegisterUser(User user);
    }
}
